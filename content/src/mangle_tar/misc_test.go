//	© 2017 Paul John King (paul_john_king@web.de).  All rights reserved.
//
//	This program is free software: you can redistribute it and/or modify it
//	under the terms of the GNU General Public License, version 3 as published
//	by the Free Software Foundation.
//
//	This program is distributed in the hope that it will be useful, but WITHOUT
//	ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//	more details.
//
//	You should have received a copy of the GNU General Public License along
//	with this program.  If not, see <http://www.gnu.org/licenses/>.

package mangle_tar

import (
	"io"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

func init() {
	log.SetFlags(0)
	log.SetOutput(os.Stderr)
	log.SetPrefix(``)

	return
}

func CaptureOutput(reader io.Reader, errorChannel chan<- error, stringChannel chan<- string) {
	var (
		buffer []byte
		err    error
	)

	buffer, err = ioutil.ReadAll(reader)
	errorChannel <- err
	stringChannel <- strings.TrimRight(string(buffer), "\n\r")

	return
}

func CaptureStdoutStderr(function func()) (receivedStdout string, receivedStderr string, err error) {
	var (
		stdout              *os.File
		stderr              *os.File
		stdoutReader        *os.File
		stdoutWriter        *os.File
		stderrReader        *os.File
		stderrWriter        *os.File
		stdoutErrorChannel  chan error
		stdoutStringChannel chan string
		stderrErrorChannel  chan error
		stderrStringChannel chan string
	)

	stdout = os.Stdout
	defer func() {
		os.Stdout = stdout
		return
	}()

	stderr = os.Stderr
	defer func() {
		os.Stderr = stderr
		log.SetOutput(stderr)
		return
	}()

	stdoutErrorChannel = make(chan error)
	stdoutStringChannel = make(chan string)
	defer func() {
		err = <-stdoutErrorChannel
		receivedStdout = <-stdoutStringChannel
		return
	}()

	stderrErrorChannel = make(chan error)
	stderrStringChannel = make(chan string)
	defer func() {
		err = <-stderrErrorChannel
		receivedStderr = <-stderrStringChannel
		return
	}()

	stdoutReader, stdoutWriter, err = os.Pipe()
	if err != nil {
		return
	}
	defer stdoutWriter.Close()
	os.Stdout = stdoutWriter

	stderrReader, stderrWriter, err = os.Pipe()
	if err != nil {
		return
	}
	defer stderrWriter.Close()
	os.Stderr = stderrWriter
	log.SetOutput(stderrWriter)

	go CaptureOutput(stdoutReader, stdoutErrorChannel, stdoutStringChannel)
	go CaptureOutput(stderrReader, stderrErrorChannel, stderrStringChannel)

	function()

	return
}

/*
© 2017 Paul John King (paul_john_king@web.de).  All rights reserved.

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License, version 3 as published by the Free
Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package mangle_tar

import (
	"encoding/xml"
	"fmt"
	"io"
)

type DirectoryElement struct {
	NameSet bool
	ModeSet bool
	UidSet  bool
	GidSet  bool
	Name    string
	Mode    string
	Uid     string
	Gid     string
	Coda    *Coda
}

func (directoryElement *DirectoryElement) UnmarshalXML(decoder *xml.Decoder, element xml.StartElement) (err error) {
	directoryElement.Coda = new(Coda)

	err = directoryElement.UnmarshalAttributes(element)
	if err != nil {
		return
	}
	err = directoryElement.UnmarshalComponents(decoder, element)
	if err != nil {
		return
	}

	return
}

func (directoryElement *DirectoryElement) UnmarshalAttributes(element xml.StartElement) (err error) {
	for _, attribute := range element.Attr {
		switch attribute.Name.Local {
		default:
			err = fmt.Errorf(
				"element '<%s>' cannot have attribute '%s'",
				element.Name.Local,
				attribute.Name.Local,
			)
			return
		}
	}

	return
}

func (directoryElement *DirectoryElement) UnmarshalComponents(decoder *xml.Decoder, element xml.StartElement) (err error) {
	var (
		token xml.Token
	)

	for {
		token, err = decoder.Token()
		if err == io.EOF {
			err = nil
			break
		}
		if err != nil {
			return
		}
		switch component := token.(type) {
		case xml.CharData:
		case xml.Comment:
		case xml.EndElement:
		case xml.ProcInst:
			err = fmt.Errorf(
				"processing instruction '<?%s %s?>' cannot occur in element '<%s>'",
				component.Target,
				component.Inst,
				element.Name.Local,
			)
			return
		case xml.StartElement:
			err = directoryElement.UnmarshalChild(decoder, element, component)
			if err != nil {
				return
			}
		}
	}

	if !directoryElement.NameSet {
		err = fmt.Errorf(
			"element '<name>' must occur in element '<%s>'",
			element.Name.Local,
		)
		return
	}

	directoryElement.Coda, err = NewDirectoryCoda(directoryElement.Name, directoryElement.Mode, directoryElement.Uid, directoryElement.Gid)
	if err != nil {
		return
	}

	return
}

func (directoryElement *DirectoryElement) UnmarshalChild(decoder *xml.Decoder, parent xml.StartElement, element xml.StartElement) (err error) {
	switch element.Name.Local {
	case "name":
		err = directoryElement.UnmarshalNameChild(decoder, parent, element)
		if err != nil {
			return
		}
	case "mode":
		err = directoryElement.UnmarshalModeChild(decoder, parent, element)
		if err != nil {
			return
		}
	case "uid":
		err = directoryElement.UnmarshalUidChild(decoder, parent, element)
		if err != nil {
			return
		}
	case "gid":
		err = directoryElement.UnmarshalGidChild(decoder, parent, element)
		if err != nil {
			return
		}
	default:
		err = fmt.Errorf(
			"element '<%s>' cannot occur in element '<%s>'",
			element.Name.Local,
			parent.Name.Local,
		)
		return
	}

	return
}

func (directoryElement *DirectoryElement) UnmarshalNameChild(decoder *xml.Decoder, parent xml.StartElement, element xml.StartElement) (err error) {
	var (
		parameter *Parameter
	)

	if directoryElement.NameSet {
		err = fmt.Errorf(
			"element '<%s>' cannot occur more than once in element '<%s>'",
			element.Name.Local,
			parent.Name.Local,
		)
		return
	}
	if directoryElement.ModeSet {
		err = fmt.Errorf(
			"element '<mode>' cannot precede element '<%s>' in element '<%s>'",
			element.Name.Local,
			parent.Name.Local,
		)
		return
	}
	if directoryElement.UidSet {
		err = fmt.Errorf(
			"element '<uid>' cannot precede element '<%s>' in element '<%s>'",
			element.Name.Local,
			parent.Name.Local,
		)
		return
	}
	if directoryElement.GidSet {
		err = fmt.Errorf(
			"element '<gid>' cannot precede element '<%s>' in element '<%s>'",
			element.Name.Local,
			parent.Name.Local,
		)
		return
	}

	directoryElement.NameSet = true
	parameter = new(Parameter)
	err = decoder.DecodeElement(parameter, &element)
	if err != nil {
		return
	}
	directoryElement.Name = parameter.Value

	return
}

func (directoryElement *DirectoryElement) UnmarshalModeChild(decoder *xml.Decoder, parent xml.StartElement, element xml.StartElement) (err error) {
	var (
		parameter *Parameter
	)

	if directoryElement.ModeSet {
		err = fmt.Errorf(
			"element '<%s>' cannot occur more than once in element '<%s>'",
			element.Name.Local,
			parent.Name.Local,
		)
		return
	}
	if directoryElement.UidSet {
		err = fmt.Errorf(
			"element '<uid>' cannot precede element '<%s>' in element '<%s>'",
			element.Name.Local,
			parent.Name.Local,
		)
		return
	}
	if directoryElement.GidSet {
		err = fmt.Errorf(
			"element '<gid>' cannot precede element '<%s>' in element '<%s>'",
			element.Name.Local,
			parent.Name.Local,
		)
		return
	}

	directoryElement.ModeSet = true
	parameter = new(Parameter)
	err = decoder.DecodeElement(parameter, &element)
	if err != nil {
		return
	}
	directoryElement.Mode = parameter.Value

	return
}

func (directoryElement *DirectoryElement) UnmarshalUidChild(decoder *xml.Decoder, parent xml.StartElement, element xml.StartElement) (err error) {
	var (
		parameter *Parameter
	)

	if directoryElement.UidSet {
		err = fmt.Errorf(
			"element '<%s>' cannot occur more than once in element '<%s>'",
			element.Name.Local,
			parent.Name.Local,
		)
		return
	}
	if directoryElement.GidSet {
		err = fmt.Errorf(
			"element '<gid>' cannot precede element '<%s>' in element '<%s>'",
			element.Name.Local,
			parent.Name.Local,
		)
		return
	}

	directoryElement.UidSet = true
	parameter = new(Parameter)
	err = decoder.DecodeElement(parameter, &element)
	if err != nil {
		return
	}
	directoryElement.Uid = parameter.Value

	return
}

func (directoryElement *DirectoryElement) UnmarshalGidChild(decoder *xml.Decoder, parent xml.StartElement, element xml.StartElement) (err error) {
	var (
		parameter *Parameter
	)

	if directoryElement.GidSet {
		err = fmt.Errorf(
			"element '<%s>' cannot occur more than once in element '<%s>'",
			element.Name.Local,
			parent.Name.Local,
		)
		return
	}

	directoryElement.GidSet = true
	parameter = new(Parameter)
	err = decoder.DecodeElement(parameter, &element)
	if err != nil {
		return
	}
	directoryElement.Gid = parameter.Value

	return
}

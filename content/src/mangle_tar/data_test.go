//	© 2017 Paul John King (paul_john_king@web.de).  All rights reserved.
//
//	This program is free software: you can redistribute it and/or modify it
//	under the terms of the GNU General Public License, version 3 as published
//	by the Free Software Foundation.
//
//	This program is distributed in the hope that it will be useful, but WITHOUT
//	ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//	more details.
//
//	You should have received a copy of the GNU General Public License along
//	with this program.  If not, see <http://www.gnu.org/licenses/>.

package mangle_tar

type (
	CodaDatum struct {
		Description     string
		InputName       string
		InputMode       string
		InputUid        string
		InputGid        string
		ExpectedSuccess bool
		ExpectedName    string
		ExpectedMode    int64
		ExpectedUid     int
		ExpectedGid     int
	}
	CodaData []CodaDatum
)

var (
	codaData CodaData = CodaData{
		{
			`standard`,
			`echo`, `0775`, `12345`, `67890`,
			true,
			`echo`, 0775, 12345, 67890,
		},

		//	Name tests
		{
			`name_is_empty`,
			``, `0775`, `12345`, `67890`,
			true,
			``, 0775, 12345, 67890,
		},
		{
			`name_contains_unicode`,
			`♠ ♣ ♥ ♦`, `0775`, `12345`, `67890`,
			true,
			`♠ ♣ ♥ ♦`, 0775, 12345, 67890,
		},
		{
			`name_contains_directory_separator`,
			`directory/file`, `0775`, `12345`, `67890`,
			true,
			`directory/file`, 0775, 12345, 67890,
		},
		{
			`name_contains_whitespace`,
			`    white    space    `, `0775`, `12345`, `67890`,
			true,
			`    white    space    `, 0775, 12345, 67890,
		},
		{
			`name_contains_tab_space`,
			`	tab	space	`, `0775`, `12345`, `67890`,
			true,
			`	tab	space	`, 0775, 12345, 67890,
		},
		{
			`name_contains_new_line`,
			`
new
line
`, `0775`, `12345`, `67890`,
			true,
			`
new
line
`, 0775, 12345, 67890,
		},

		//	Mode tests
		{
			`mode_is_empty`,
			`echo`, ``, `12345`, `67890`,
			true,
			`echo`, 00000, 12345, 67890,
		},
		{
			`mode_is_null`,
			`echo`, `0000`, `12345`, `67890`,
			true,
			`echo`, 00000, 12345, 67890,
		},
		{
			`mode_contains_other_access_bit`,
			`echo`, `0001`, `12345`, `67890`,
			true,
			`echo`, 00001, 12345, 67890,
		},
		{
			`mode_contains_other_write_bit`,
			`echo`, `0002`, `12345`, `67890`,
			true,
			`echo`, 00002, 12345, 67890,
		},
		{
			`mode_contains_other_read_bit`,
			`echo`, `0004`, `12345`, `67890`,
			true,
			`echo`, 00004, 12345, 67890,
		},
		{
			`mode_contains_group_access_bit`,
			`echo`, `0010`, `12345`, `67890`,
			true,
			`echo`, 00010, 12345, 67890,
		},
		{
			`mode_contains_group_write_bit`,
			`echo`, `0020`, `12345`, `67890`,
			true,
			`echo`, 00020, 12345, 67890,
		},
		{
			`mode_contains_group_read_bit`,
			`echo`, `0040`, `12345`, `67890`,
			true,
			`echo`, 00040, 12345, 67890,
		},
		{
			`mode_contains_owner_access_bit`,
			`echo`, `0100`, `12345`, `67890`,
			true,
			`echo`, 00100, 12345, 67890,
		},
		{
			`mode_contains_owner_write_bit`,
			`echo`, `0200`, `12345`, `67890`,
			true,
			`echo`, 00200, 12345, 67890,
		},
		{
			`mode_contains_owner_read_bit`,
			`echo`, `0400`, `12345`, `67890`,
			true,
			`echo`, 00400, 12345, 67890,
		},
		{
			`mode_contains_sticky_bit`,
			`echo`, `1000`, `12345`, `67890`,
			true,
			`echo`, 01000, 12345, 67890,
		},
		{
			`mode_contains_setgid_bit`,
			`echo`, `2000`, `12345`, `67890`,
			true,
			`echo`, 02000, 12345, 67890,
		},
		{
			`mode_contains_setuid_bit`,
			`echo`, `4000`, `12345`, `67890`,
			true,
			`echo`, 04000, 12345, 67890,
		},
		{
			`mode_contains_non-numbers`,
			`echo`, `xxxx`, `12345`, `67890`,
			false,
			``, 0, 0, 0,
		},
		{
			`mode_contains_non-octal_number`,
			`echo`, `0009`, `12345`, `67890`,
			false,
			``, 0, 0, 0,
		},
		{
			`mode_contains_too_few_digits`,
			`echo`, `775`, `12345`, `67890`,
			false,
			``, 0, 0, 0,
		},
		{
			`mode_contains_too_many_digits`,
			`echo`, `00775`, `12345`, `67890`,
			false,
			``, 0, 0, 0,
		},
		{
			`mode_is_negative_octal_number`,
			`echo`, `-775`, `12345`, `67890`,
			false,
			``, 0, 0, 0,
		},

		//	User ID tests
		{
			`uid_is_empty`,
			`echo`, `0775`, ``, `67890`,
			true,
			`echo`, 00775, 0, 67890,
		},
		{
			`uid_is_zero`,
			`echo`, `0775`, `0`, `67890`,
			true,
			`echo`, 00775, 0, 67890,
		},
		{
			`uid_contains_non-numbers`,
			`echo`, `0775`, `nobody`, `67890`,
			false,
			``, 0, 0, 0,
		},
		{
			`uid_is_negative_number`,
			`echo`, `0775`, `-12345`, `67890`,
			false,
			``, 0, 0, 0,
		},

		//	Group ID tests
		{
			`gid_is_empty`,
			`echo`, `0775`, `12345`, ``,
			true,
			`echo`, 00775, 12345, 0,
		},
		{
			`gid_is_zero`,
			`echo`, `0775`, `12345`, `0`,
			true,
			`echo`, 00775, 12345, 0,
		},
		{
			`gid_contains_non-numbers`,
			`echo`, `0775`, `12345`, `nogroup`,
			false,
			``, 0, 0, 0,
		},
		{
			`gid_is_negative_number`,
			`echo`, `0775`, `12345`, `-67890`,
			false,
			``, 0, 0, 0,
		},
	}
)

type (
	CodasDatum struct {
		Description string
		Names       []string
	}
	CodasData []CodasDatum
)

var (
	codasData CodasData = CodasData{
		{
			`list_is_empty`,
			[]string{},
		},
		{
			`list_is_alpha`,
			[]string{`alpha`},
		},
		{
			`list_is_alpha_alpha`,
			[]string{`alpha`, `alpha`},
		},
		{
			`list_is_alpha_bravo_charlie`,
			[]string{`alpha`, `bravo`, `charlie`},
		},
		{
			`names_is_empty`,
			[]string{``},
		},
		{
			`names_contains_unicode`,
			[]string{`♠ ♣ ♥ ♦`},
		},
		{
			`name_contains_directory_separator`,
			[]string{`directory/file`},
		},
		{
			`name_contains_whitespace`,
			[]string{`    white    space    `},
		},
		{
			`name_contains_tab_space`,
			[]string{`	tab	space	`},
		},
		{
			`name_contains_new_line`,
			[]string{`
new
line
`},
		},
	}
)

type (
	FilterKeepAsDatum struct {
		Description     string
		InputKeep       string
		InputAs         string
		ExpectedSuccess bool
	}
	FilterKeepAsData []FilterKeepAsDatum
)

var (
	filterKeepAsData FilterKeepAsData = FilterKeepAsData{
		{
			`^alpha/»ALPHA`,
			`^alpha/`, `ALPHA`,
			true,
		},
	}
)

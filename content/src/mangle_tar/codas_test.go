//	© 2017 Paul John King (paul_john_king@web.de).  All rights reserved.
//
//	This program is free software: you can redistribute it and/or modify it
//	under the terms of the GNU General Public License, version 3 as published
//	by the Free Software Foundation.
//
//	This program is distributed in the hope that it will be useful, but WITHOUT
//	ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//	more details.
//
//	You should have received a copy of the GNU General Public License along
//	with this program.  If not, see <http://www.gnu.org/licenses/>.

package mangle_tar

import (
	"archive/tar"
	"fmt"
	"io"
	"strings"
	"testing"
)

func Test_Codas_Apply(t *testing.T) {
	for _, datum := range codasData {
		t.Run(
			datum.Description,
			func(t *testing.T) {

				var (
					codas                 *Codas
					err                   error
					report                string
					passed                bool
					success               bool
					reader                io.Reader
					writer                io.Writer
					producerErrorChannel  chan error
					producerReportChannel chan string
					producerPassedChannel chan bool
					consumerErrorChannel  chan error
					consumerReportChannel chan string
					consumerPassedChannel chan bool
				)

				codas = new(Codas)
				err = nil
				report = ``
				passed = true
				success = true
				reader, writer = io.Pipe()
				producerErrorChannel = make(chan error)
				producerReportChannel = make(chan string)
				producerPassedChannel = make(chan bool)
				consumerErrorChannel = make(chan error)
				consumerReportChannel = make(chan string)
				consumerPassedChannel = make(chan bool)

				for _, name := range datum.Names {
					_coda, _report, _passed, _success, err := createCoda(name)
					if err != nil {
						t.Fatalf(`error: %s`, err.Error())
					}
					*codas = append(*codas, _coda)
					report = report + _report
					passed = passed && _passed
					success = success && _success
				}
				if !(passed && success) {
					t.Fatal(report)
				}

				go produceTarObjects(datum.Names, codas, writer, producerErrorChannel, producerReportChannel, producerPassedChannel)
				go consumeTarObjects(datum.Names, reader, consumerErrorChannel, consumerReportChannel, consumerPassedChannel)

				err = <-producerErrorChannel
				if err != nil {
					t.Fatalf(`error: %s`, err.Error())
				}
				report = report + <-producerReportChannel
				passed = passed && <-producerPassedChannel
				if !passed {
					t.Fatal(report)
				}

				err = <-consumerErrorChannel
				if err != nil {
					t.Fatalf(`error: %s`, err.Error())
				}
				report = report + <-consumerReportChannel
				passed = passed && <-consumerPassedChannel
				if !passed {
					t.Fatal(report)
				}

				t.Log(report)

				return
			},
		)
	}
}

func createCoda(name string) (coda *Coda, report string, passed bool, success bool, err error) {
	var (
		expectedStdout  string
		expectedStderr  string
		expectedSuccess bool

		receivedStdout  string
		receivedStderr  string
		receivedError   error
		receivedSuccess bool
	)

	expectedStdout = ``
	expectedStderr = fmt.Sprintf(
		`initialising add directory '%s' with mode '', user ID '' and group ID ''`,
		name,
	)
	expectedSuccess = true

	receivedStdout, receivedStderr, err = CaptureStdoutStderr(
		func() {
			coda, receivedError = NewDirectoryCoda(name, ``, ``, ``)
		},
	)
	if err != nil {
		return
	}
	receivedSuccess = receivedError == nil

	report = fmt.Sprintf(`
┍━━━━━━━━━━━━━━━━┯━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
│ execute        │ NewDirectoryCoda('%s', '%s', '%s', '%s')
┝━━━━━━━━━━━━━━━━┿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
│ stdout         │
│   expected     │ '%s'
│   received     │ '%s'
│   match        │ %t
├────────────────┼────────────────────────────────────────────────
│ stderr         │
│   expected     │ '%s'
│   received     │ '%s'
│   match        │ %t
├────────────────┼────────────────────────────────────────────────
│ success        │
│   expected     │ %t
│   received     │ %t
│   match        │ %t
┕━━━━━━━━━━━━━━━━┷━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━`,
		name, ``, ``, ``,
		expectedStdout, receivedStdout, expectedStdout == receivedStdout,
		expectedStderr, receivedStderr, expectedStderr == receivedStderr,
		expectedSuccess, receivedSuccess, expectedSuccess == receivedSuccess,
	)
	passed = expectedStdout == receivedStdout &&
		expectedStderr == receivedStderr &&
		expectedSuccess == receivedSuccess
	success = expectedSuccess && receivedSuccess

	return
}

func produceTarObjects(names []string, codas *Codas, writer io.Writer, errorChannel chan<- error, reportChannel chan<- string, passedChannel chan<- bool) {
	var (
		err       error
		tarWriter *tar.Writer

		expectedStdout  string
		expectedStderr  string
		expectedSuccess bool

		receivedStdout  string
		receivedStderr  string
		receivedError   error
		receivedSuccess bool
	)

	tarWriter = tar.NewWriter(writer)
	defer tarWriter.Close()

	expectedStdout = ``
	expectedStderr = ``
	for _, name := range names {
		expectedStderr = expectedStderr + fmt.Sprintf(
			"adding directory '%s' with mode '', user ID '' and group ID ''\n",
			name,
		)
	}
	expectedStderr = strings.TrimRight(expectedStderr, "\n\r")
	expectedSuccess = true

	receivedStdout, receivedStderr, err = CaptureStdoutStderr(
		func() {
			receivedError = codas.Apply(tarWriter)
		},
	)
	if err != nil {
		errorChannel <- err
		reportChannel <- ``
		passedChannel <- true
		return
	}
	receivedSuccess = nil == receivedError

	errorChannel <- err
	reportChannel <- fmt.Sprintf(`
┍━━━━━━━━━━━━━━━━┑
│ apply codas    │
┝━━━━━━━━━━━━━━━━┿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
│ stdout         │
│   expected     │ '%s'
│   received     │ '%s'
│   match        │ %t
├────────────────┼────────────────────────────────────────────────
│ stderr         │
│   expected     │ '%s'
│   received     │ '%s'
│   match        │ %t
├────────────────┼────────────────────────────────────────────────
│ success        │
│   expected     │ %t
│   received     │ %t
│   match        │ %t
┕━━━━━━━━━━━━━━━━┷━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━`,
		expectedStdout, receivedStdout, expectedStdout == receivedStdout,
		expectedStderr, receivedStderr, expectedStderr == receivedStderr,
		expectedSuccess, receivedSuccess, expectedSuccess == receivedSuccess,
	)
	passedChannel <- expectedStdout == receivedStdout &&
		expectedStderr == receivedStderr &&
		expectedSuccess == receivedSuccess

	return
}

func consumeTarObjects(names []string, reader io.Reader, errorChannel chan<- error, reportChannel chan<- string, passedChannel chan<- bool) {
	var (
		err       error
		_err      error
		report    string
		passed    bool
		tarReader *tar.Reader
		tarHeader *tar.Header
		excess    int
	)

	err = nil
	report = `
┍━━━━━━━━━━━━━━━━┑
│ examine tar    │`
	passed = true

	tarReader = tar.NewReader(reader)

	for _, expectedName := range names {
		var (
			receivedName string
		)

		tarHeader, _err = tarReader.Next()
		if _err == io.EOF {
			report = report + `
┝━━━━━━━━━━━━━━━━┿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
│ failed         │ the tar contains too few objects`

			passed = passed && false
			break
		}
		if err == nil {
			err = _err
		}

		receivedName = tarHeader.Name
		report = report + fmt.Sprintf(`
┝━━━━━━━━━━━━━━━━┿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
│ name           │
│   expected     │ '%s'
│   received     │ '%s'
│   match        │ %t`,
			expectedName, receivedName, expectedName == receivedName,
		)
		passed = passed && expectedName == receivedName
	}

	for {
		_, _err = tarReader.Next()
		if _err == io.EOF {
			break
		}
		if err == nil {
			err = _err
		}
		excess++
	}

	if excess > 0 {
		report = report + fmt.Sprintf(`
┝━━━━━━━━━━━━━━━━┿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
│ failed         │ the tar contains %d too many objects`,
			excess,
		)
		passed = passed && false
	}

	report = report + `
┕━━━━━━━━━━━━━━━━┷━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━`

	errorChannel <- err
	reportChannel <- report
	passedChannel <- passed

	return
}

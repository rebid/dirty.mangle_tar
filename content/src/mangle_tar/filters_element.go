/*
© 2017 Paul John King (paul_john_king@web.de).  All rights reserved.

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License, version 3 as published by the Free
Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package mangle_tar

import (
	"encoding/xml"
	"fmt"
	"io"
)

type FiltersElement struct {
	Filters       *Filters
	DefaultFilter *Filter
}

func (filtersElement *FiltersElement) UnmarshalXML(decoder *xml.Decoder, element xml.StartElement) (err error) {
	filtersElement.Filters = new(Filters)

	err = filtersElement.UnmarshalAttributes(element)
	if err != nil {
		return
	}
	err = filtersElement.UnmarshalComponents(decoder, element)
	if err != nil {
		return
	}
	err = filtersElement.Postprocess(element)
	if err != nil {
		return
	}

	return
}

func (filtersElement *FiltersElement) UnmarshalAttributes(element xml.StartElement) (err error) {
	for _, attribute := range element.Attr {
		switch attribute.Name.Local {
		case "keep-by-default":
			err = filtersElement.UnmarshalKeepByDefaultValues(attribute)
			if err != nil {
				return
			}
		default:
			err = fmt.Errorf(
				"string '%s' is not a valid attribute for element '<%s>'",
				attribute.Name.Local,
				element.Name.Local,
			)
			return
		}
	}

	return
}

func (filtersElement *FiltersElement) UnmarshalKeepByDefaultValues(attribute xml.Attr) (err error) {
	switch attribute.Value {
	case "true":
		filtersElement.DefaultFilter = NewKeepByDefaultFilter()
	case "1":
		filtersElement.DefaultFilter = NewKeepByDefaultFilter()
	case "false":
		filtersElement.DefaultFilter = NewDropByDefaultFilter()
	case "0":
		filtersElement.DefaultFilter = NewDropByDefaultFilter()
	default:
		err = fmt.Errorf(
			"string '%s' is not a valid value for attribute '%s'",
			attribute.Value,
			attribute.Name.Local,
		)
		return
	}

	return
}

func (filtersElement *FiltersElement) UnmarshalComponents(decoder *xml.Decoder, element xml.StartElement) (err error) {
	var (
		token xml.Token
	)

	for {
		token, err = decoder.Token()
		if err == io.EOF {
			err = nil
			break
		}
		if err != nil {
			return
		}
		switch component := token.(type) {
		case xml.CharData:
		case xml.Comment:
		case xml.EndElement:
		case xml.ProcInst:
			err = fmt.Errorf(
				"processing instruction '<?%s %s?>' is not a valid component of element '<%s>'",
				component.Target,
				component.Inst,
				element.Name.Local,
			)
			return
		case xml.StartElement:
			err = filtersElement.UnmarshalChild(decoder, element, component)
			if err != nil {
				return
			}
		}
	}

	return
}

func (filtersElement *FiltersElement) UnmarshalChild(decoder *xml.Decoder, parent xml.StartElement, element xml.StartElement) (err error) {
	var (
		filterElement *FilterElement
	)

	filterElement = new(FilterElement)
	switch element.Name.Local {
	case "filter":
		err = decoder.DecodeElement(filterElement, &element)
		if err != nil {
			return
		}
	default:
		err = fmt.Errorf(
			"element '<%s>' is not a valid component of element '<%s>'",
			element.Name.Local,
			parent.Name.Local,
		)
		return
	}
	*filtersElement.Filters = append(*filtersElement.Filters, filterElement.Filter)

	return
}

func (filtersElement *FiltersElement) Postprocess(element xml.StartElement) (err error) {
	if filtersElement.DefaultFilter == nil {
		filtersElement.DefaultFilter = NewDropByDefaultFilter()
	}
	*filtersElement.Filters = append(*filtersElement.Filters, filtersElement.DefaultFilter)

	return
}

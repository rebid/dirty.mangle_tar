//	© 2017 Paul John King (paul_john_king@web.de).  All rights reserved.
//
//	This program is free software: you can redistribute it and/or modify it
//	under the terms of the GNU General Public License, version 3 as published
//	by the Free Software Foundation.
//
//	This program is distributed in the hope that it will be useful, but WITHOUT
//	ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//	more details.
//
//	You should have received a copy of the GNU General Public License along
//	with this program.  If not, see <http://www.gnu.org/licenses/>.

package mangle_tar

import (
	"archive/tar"
)

type Filters []*Filter

func (filters *Filters) Apply(header *tar.Header, reader *tar.Reader, writer *tar.Writer) (err error) {
	var (
		applied bool
	)

	for _, filter := range *filters {
		applied, err = filter.Apply(header, reader, writer)
		if err != nil {
			return
		}
		if applied {
			break
		}
	}

	return
}

//	© 2017 Paul John King (paul_john_king@web.de).  All rights reserved.
//
//	This program is free software: you can redistribute it and/or modify it
//	under the terms of the GNU General Public License, version 3 as published
//	by the Free Software Foundation.
//
//	This program is distributed in the hope that it will be useful, but WITHOUT
//	ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//	more details.
//
//	You should have received a copy of the GNU General Public License along
//	with this program.  If not, see <http://www.gnu.org/licenses/>.

package mangle_tar

import (
	"archive/tar"
	"io"
	"log"
	"regexp"
)

//	A 'Filter' instance has an 'Apply' method that determines whether it
//	applies to a tar object read from a tar reader and, if so, may try to write
//	a tar object (for example, the read tar object with a modified header or
//	content) to a tar writer.
type Filter struct {
	//	'Apply' determines whether it applies to a tar object with the tar
	//	header 'header' read from the tar reader 'reader'.  If the method does
	//	apply then it may try to write a tar object (for example, the tar
	//	object read from 'reader' with a modified header or content) to the tar
	//	writer 'writer' and return 'applied' set to 'true'.
	Apply func(header *tar.Header, reader *tar.Reader, writer *tar.Writer) (applied bool, err error)
}

//	'NewKeepAsFilter' tries to return a 'Filter' instance 'filter' whose
//	'Apply' method applies to a tar object read from a tar reader if the
//	regular expression 'keep' matches the name or (if the object is a hard or
//	symbolic link) the link name of the object.  If the method does apply then
//	it copies the tar object to a tar writer, but with a name and link name
//	(where appropriate) formed by replacing each match of 'keep' with the
//	replacement text 'as'.  Each '$' character in 'as' is interpreted as in the
//	the 'Expand' method of the Golang 'regexp.Regexp' type -- see
//
//	    https://golang.org/pkg/regexp/#Regexp.Expand
//
//	for details.
//
//	FIXME: This method does *not* yet pattern match against and, where a match
//	occurs, modify the link name of hard and symbolic link objects.
func NewKeepAsFilter(keep string, as string) (filter *Filter, err error) {
	var (
		pattern *regexp.Regexp
	)

	log.Printf(`initialising keep '%s' as '%s'`, keep, as)
	pattern, err = regexp.Compile(keep)
	if err != nil {
		return
	}

	filter = new(Filter)
	filter.Apply = func(header *tar.Header, reader *tar.Reader, writer *tar.Writer) (applied bool, err error) {
		var (
			name string
		)

		if pattern.MatchString(header.Name) {
			name = pattern.ReplaceAllString(header.Name, as)
			if len(name) == 0 {
				return
			}
			log.Printf(`keeping '%s' as '%s'`, header.Name, name)
			header.Name = name
			err = writer.WriteHeader(header)
			if err != nil {
				return
			}
			_, err = io.Copy(writer, reader)
			if err != nil {
				return
			}
			applied = true
		}

		return
	}

	return
}

func NewKeepFilter(keep string) (filter *Filter, err error) {
	var (
		pattern *regexp.Regexp
	)

	log.Printf(`initialising keep '%s'`, keep)
	pattern, err = regexp.Compile(keep)
	if err != nil {
		return
	}

	filter = new(Filter)
	filter.Apply = func(header *tar.Header, reader *tar.Reader, writer *tar.Writer) (applied bool, err error) {
		if pattern.MatchString(header.Name) {
			log.Printf(`keeping '%s'`, header.Name)
			err = writer.WriteHeader(header)
			if err != nil {
				return
			}
			_, err = io.Copy(writer, reader)
			if err != nil {
				return
			}
			applied = true
		}

		return
	}

	return
}

func NewDropFilter(drop string) (filter *Filter, err error) {
	var (
		pattern *regexp.Regexp
	)

	log.Printf(`initialising drop '%s'`, drop)
	pattern, err = regexp.Compile(drop)
	if err != nil {
		return
	}

	filter = new(Filter)
	filter.Apply = func(header *tar.Header, reader *tar.Reader, writer *tar.Writer) (applied bool, err error) {
		if pattern.MatchString(header.Name) {
			log.Printf(`dropping '%s'`, header.Name)
			applied = true
		}

		return
	}

	return
}

func NewKeepByDefaultFilter() (filter *Filter) {
	log.Printf(`initialising keep-by-default`)

	filter = new(Filter)
	filter.Apply = func(header *tar.Header, reader *tar.Reader, writer *tar.Writer) (applied bool, err error) {
		log.Printf(`keeping '%s'`, header.Name)
		err = writer.WriteHeader(header)
		if err != nil {
			return
		}
		_, err = io.Copy(writer, reader)
		if err != nil {
			return
		}
		applied = true

		return
	}

	return
}

func NewDropByDefaultFilter() (filter *Filter) {
	log.Printf(`initialising drop-by-default`)

	filter = new(Filter)
	filter.Apply = func(header *tar.Header, reader *tar.Reader, writer *tar.Writer) (applied bool, err error) {
		log.Printf(`dropping '%s'`, header.Name)
		applied = true

		return
	}

	return
}

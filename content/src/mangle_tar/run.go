/*
© 2017 Paul John King (paul_john_king@web.de).  All rights reserved.

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License, version 3 as published by the Free
Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package mangle_tar

import (
	"archive/tar"
	"io"
	"os"
)

func Run(configuration string) (err error) {
	var (
		filters *Filters
		codas   *Codas
		reader  *tar.Reader
		writer  *tar.Writer
		header  *tar.Header
	)

	filters, codas, err = NewMangleTar(configuration)
	if err != nil {
		return
	}
	reader = tar.NewReader(os.Stdin)
	writer = tar.NewWriter(os.Stdout)
	defer func(writer *tar.Writer, err *error) {
		var (
			_err error
		)

		_err = writer.Close()
		if *err == nil {
			*err = _err
		}
		if *err != nil {
			return
		}
		return
	}(writer, &err)

	for {
		header, err = reader.Next()
		if err == io.EOF {
			err = nil
			break
		}
		if err != nil {
			return
		}
		err = filters.Apply(header, reader, writer)
		if err != nil {
			return
		}
	}
	err = codas.Apply(writer)
	if err != nil {
		return
	}

	return
}

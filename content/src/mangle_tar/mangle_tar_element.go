/*
© 2017 Paul John King (paul_john_king@web.de).  All rights reserved.

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License, version 3 as published by the Free
Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package mangle_tar

import (
	"encoding/xml"
	"fmt"
	"io"
	"strings"
)

type MangleTarElement struct {
	FiltersElement       *FiltersElement
	FiltersElementParsed bool
	CodasElement         *CodasElement
	CodasElementParsed   bool
	Filters              *Filters
	Codas                *Codas
}

func (mangleTarElement *MangleTarElement) UnmarshalXML(decoder *xml.Decoder, element xml.StartElement) (err error) {
	mangleTarElement.FiltersElement = new(FiltersElement)
	mangleTarElement.CodasElement = new(CodasElement)
	mangleTarElement.Filters = new(Filters)
	mangleTarElement.Codas = new(Codas)

	err = mangleTarElement.UnmarshalAttributes(element)
	if err != nil {
		return
	}
	err = mangleTarElement.UnmarshalComponents(decoder, element)
	if err != nil {
		return
	}

	return
}

func (mangleTarElement *MangleTarElement) UnmarshalAttributes(element xml.StartElement) (err error) {
	for _, attribute := range element.Attr {
		switch attribute.Name.Local {
		default:
			err = fmt.Errorf(
				"'%s' is not a valid attribute for element '<%s>'",
				attribute.Name.Local,
				element.Name.Local,
			)
			return
		}
	}

	return
}

func (mangleTarElement *MangleTarElement) UnmarshalComponents(decoder *xml.Decoder, element xml.StartElement) (err error) {
	var (
		token xml.Token
	)

	for {
		token, err = decoder.Token()
		if err == io.EOF {
			err = nil
			break
		}
		if err != nil {
			return
		}
		switch component := token.(type) {
		case xml.CharData:
		case xml.Comment:
		case xml.EndElement:
		case xml.ProcInst:
		case xml.StartElement:
			err = mangleTarElement.UnmarshalChildElement(decoder, element, component)
			if err != nil {
				return
			}
		}
	}

	return
}

func (mangleTarElement *MangleTarElement) UnmarshalChildElement(decoder *xml.Decoder, parent xml.StartElement, element xml.StartElement) (err error) {
	switch element.Name.Local {
	case "filters":
		if mangleTarElement.FiltersElementParsed {
			err = fmt.Errorf(
				"element '<%s>' cannot contain more than one '<%s>' element",
				parent.Name.Local,
				element.Name.Local,
			)
			return
		}
		err = decoder.DecodeElement(mangleTarElement.FiltersElement, &element)
		if err != nil {
			return
		}
		mangleTarElement.FiltersElementParsed = true
		mangleTarElement.Filters = mangleTarElement.FiltersElement.Filters
	case "codas":
		if mangleTarElement.CodasElementParsed {
			err = fmt.Errorf(
				"element '<%s>' cannot contain more than one '<%s>' element",
				parent.Name.Local,
				element.Name.Local,
			)
			return
		}
		err = decoder.DecodeElement(mangleTarElement.CodasElement, &element)
		if err != nil {
			return
		}
		mangleTarElement.CodasElementParsed = true
		mangleTarElement.Codas = mangleTarElement.CodasElement.Codas
	default:
		err = fmt.Errorf(
			"element '<%s>' is not a valid component of element '<%s>'",
			element.Name.Local,
			parent.Name.Local,
		)
		return
	}

	return
}

func NewMangleTar(configuration string) (filters *Filters, codas *Codas, err error) {
	var (
		decoder          *xml.Decoder
		mangleTarElement *MangleTarElement
		token            xml.Token
		found            bool
	)

	decoder = xml.NewDecoder(strings.NewReader(configuration))
	mangleTarElement = new(MangleTarElement)
	for {
		token, err = decoder.Token()
		if err == io.EOF {
			err = nil
			break
		}
		if err != nil {
			return
		}
		switch component := token.(type) {
		case xml.CharData:
		case xml.Comment:
		case xml.EndElement:
		case xml.ProcInst:
		case xml.StartElement:
			switch {
			case found:
				err = fmt.Errorf(
					"there are multiple top-level elements",
				)
				return
			case component.Name.Local == "mangle-tar":
				err = decoder.DecodeElement(&mangleTarElement, &component)
				if err != nil {
					return
				}
				found = true
			default:
				err = fmt.Errorf(
					"'<%s>' is not a valid top-level element",
					component.Name.Local,
				)
				return
			}
		}
	}
	if !found {
		err = fmt.Errorf(
			"there are no top-level elements",
		)
		return
	}
	filters = mangleTarElement.Filters
	codas = mangleTarElement.Codas

	return
}

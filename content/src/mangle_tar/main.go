//+build ignore

/*
© 2017 Paul John King (paul_john_king@web.de).  All rights reserved.

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License, version 3 as published by the Free
Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package main

import (
	. "mangle_tar"

	"flag"
	"fmt"
	"io"
	"log"
	"os"
)

func init() {
	log.SetFlags(0)
	log.SetOutput(os.Stderr)
	log.SetPrefix("")

	return
}

//	TODO: Update the long help message.
func main() {
	const (
		shortHelpMessage string = `Suppose this Docker image has the tag '«tag»'.

Usage
-----

The call

    docker run --interactive ... «tag» «configuration»

reads a tar from standard input, keeps or drops objects in the tar as specified
by the configuration '«configuration»', and writes the resultant tar to
standard output.

The call

    docker run ... «tag» -h|-help

writes a short help message to standard output and exits.

The call

    docker run ... «tag» -H|-Help

writes a long help message to standard output and exits.
`

		longHelpMessage string = shortHelpMessage + `
Details
-------

'«configuration»' is XML of the form

    <filters keep-by-default="«bool»">
        <filter> ... </filter>
                  :
        <filter> ... </filter>
    </filters>

An element of the form

    <filter>
        <keep>«regexp»</keep>
        <as>«text»</as>
    </filter>

specifies that each object whose name matches the regular expression '«regexp»'
be kept, but with a name formed by replacing each match of '«regexp»' with the
replacement text '«text»'.  Each '$' character in '«text»' is interpreted as in
the 'Expand' method of the Golang 'regexp.Regexp' type -- see

    https://golang.org/pkg/regexp/#Regexp.Expand

for details.  An element of the form

    <filter>
        <keep>«regexp»</keep>
    </filter>

specifies that each object whose name matches the regular expression '«regexp»'
be kept unchanged.  An element of the form

    <filter>
        <drop>«regexp»</drop>
    </filter>

specifies that each object whose name matches the regular expression '«regexp»'
be dropped.  '«regexp»' and '«text»' must be XML character data -- an error is
returned if they contain XML comments, elements or processing instructions.

Only the first '<filter>' element (in top-down order) that matches an object's
name applies to that object.  If no '<filter>' element applies to an object
then the object is kept unchanged if the optional 'keep-by-default' attribute
of the '<filters>' tag has value "true" or "1", and dropped if the attribute
has value "false" or "0" or is missing entirely.

Notes
-----

The '<filter>' elements

    <filter>
        <keep>«regexp»</keep>
    </filter>

and

    <filter>
        <keep>«regexp»</keep>
        <as></as>
    </filter>

do not have identical behaviour.  For example

    <filter>
        <keep>^fred/</keep>
    </filter>

specifies that an object with the name 'fred/flintstone' be kept unchanged,
whereas

    <filter>
        <keep>^fred/</keep>
        <as></as>
    </filter>

specifies that an object with the name 'fred/flintstone' also be kept, but with
the name 'flintstone'.
`
	)

	var (
		getMessageWriter              func(io.Writer, string) func()
		writeLongHelpMessageToStdout  func()
		writeShortHelpMessageToStdout func()
		writeShortHelpMessageToStderr func()

		err           error
		longHelpFlag  bool
		shortHelpFlag bool
	)

	getMessageWriter = func(writer io.Writer, message string) (writeMessage func()) {
		writeMessage = func() {
			fmt.Fprintf(writer, message)

			return
		}

		return
	}
	writeLongHelpMessageToStdout = getMessageWriter(os.Stdout, longHelpMessage)
	writeShortHelpMessageToStdout = getMessageWriter(os.Stdout, shortHelpMessage)
	writeShortHelpMessageToStderr = getMessageWriter(os.Stderr, shortHelpMessage)

	flag.Usage = writeShortHelpMessageToStderr
	flag.BoolVar(&longHelpFlag, `Help`, false, ``)
	flag.BoolVar(&longHelpFlag, `H`, false, ``)
	flag.BoolVar(&shortHelpFlag, `help`, false, ``)
	flag.BoolVar(&shortHelpFlag, `h`, false, ``)
	flag.Parse()

	if flag.NArg() != 1 {
		fmt.Fprintf(os.Stderr, "exactly one command-line argument is required\n")
		writeShortHelpMessageToStderr()
		os.Exit(2)
	}
	if longHelpFlag {
		writeLongHelpMessageToStdout()
		os.Exit(0)
	}
	if shortHelpFlag {
		writeShortHelpMessageToStdout()
		os.Exit(0)
	}

	err = Run(flag.Arg(0))
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err.Error())
		writeShortHelpMessageToStderr()
		os.Exit(1)
	}

	os.Exit(0)
}

/*
© 2017 Paul John King (paul_john_king@web.de).  All rights reserved.

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License, version 3 as published by the Free
Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package mangle_tar

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"io"
)

type Parameter struct {
	Name  string
	Value string
}

func (parameter *Parameter) UnmarshalXML(decoder *xml.Decoder, element xml.StartElement) (err error) {
	err = parameter.UnmarshalAttributes(element)
	if err != nil {
		return
	}
	err = parameter.UnmarshalComponents(decoder, element)
	if err != nil {
		return
	}

	return
}

func (parameter *Parameter) UnmarshalAttributes(element xml.StartElement) (err error) {
	for _, attribute := range element.Attr {
		switch attribute.Name.Local {
		default:
			err = fmt.Errorf(
				"'%s' is not a valid attribute for element '<%s>'",
				attribute.Name.Local,
				element.Name.Local,
			)
			return
		}
	}

	return
}

func (parameter *Parameter) UnmarshalComponents(decoder *xml.Decoder, element xml.StartElement) (err error) {
	var (
		token  xml.Token
		buffer bytes.Buffer
	)

	parameter.Name = element.Name.Local
	for {
		token, err = decoder.Token()
		if err == io.EOF {
			err = nil
			break
		}
		if err != nil {
			return
		}
		switch component := token.(type) {
		case xml.CharData:
			buffer.Write(component)
		case xml.Comment:
			err = fmt.Errorf(
				"comment '<!--%s-->' is not a valid component of element '<%s>'",
				component,
				element.Name.Local,
			)
			return
		case xml.EndElement:
		case xml.ProcInst:
			err = fmt.Errorf(
				"processing instruction '<?%s %s?>' is not a valid component of element '<%s>'",
				component.Target,
				component.Inst,
				element.Name.Local,
			)
			return
		case xml.StartElement:
			err = fmt.Errorf(
				"element '<%s>' is not a valid component of element '<%s>'",
				component.Name.Local,
				element.Name.Local,
			)
			return
		}
	}
	parameter.Value = buffer.String()

	return
}

type Parameters []*Parameter

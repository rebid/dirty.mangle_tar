//	© 2017 Paul John King (paul_john_king@web.de).  All rights reserved.
//
//	This program is free software: you can redistribute it and/or modify it
//	under the terms of the GNU General Public License, version 3 as published
//	by the Free Software Foundation.
//
//	This program is distributed in the hope that it will be useful, but WITHOUT
//	ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//	more details.
//
//	You should have received a copy of the GNU General Public License along
//	with this program.  If not, see <http://www.gnu.org/licenses/>.

package mangle_tar

import (
	"archive/tar"
	"fmt"
	"log"
	"regexp"
	"strconv"
)

//	A 'Coda' instance has an 'Apply' method that writes a tar object to a tar
//	writer.
type Coda struct {
	//	'Apply' tries to write a tar object to the tar writer 'writer'.
	Apply func(writer *tar.Writer) (err error)
}

//	'NewDirectoryHeader' tries to return the header 'header' of a tar directory
//	object with the name 'name', the mode 'mode', the user ID 'uid' and the
//	group ID 'gid'.  'mode' must comprise four octal digits or the empty
//	string.  If 'mode' is the empty string then the object has the mode '0000'.
//	'uid' must be a natural number or the empty string.  If 'uid' is the empty
//	string then the object has the user ID '0'.  'gid' must be a natural number
//	or the empty string.  If 'gid' is the empty string then the object has the
//	group ID '0'.
func NewDirectoryHeader(name string, mode string, uid string, gid string) (header *tar.Header, err error) {
	header = new(tar.Header)
	header.Name = name
	if mode != `` {
		var (
			matched bool
		)

		matched, err = regexp.MatchString(`^[01234567]{4,4}$`, mode)
		if err != nil {
			return
		}
		if !matched {
			err = fmt.Errorf(`mode '%s' does not comprise four octal digits`, mode)
			return
		}
		header.Mode, err = strconv.ParseInt(mode, 8, 64)
		if err != nil {
			return
		}
	}
	if uid != `` {
		header.Uid, err = strconv.Atoi(uid)
		if err != nil {
			return
		}
		if header.Uid < 0 {
			err = fmt.Errorf(`user ID '%s' is negative`, uid)
			return
		}
	}
	if gid != `` {
		header.Gid, err = strconv.Atoi(gid)
		if err != nil {
			return
		}
		if header.Gid < 0 {
			err = fmt.Errorf(`group ID '%s' is negative`, gid)
			return
		}
	}
	header.Typeflag = tar.TypeDir

	return
}

//	'NewDirectoryCoda' tries to return a 'Coda' instance 'coda' whose 'Apply'
//	method tries to write a tar directory object with the name 'name', the mode
//	'mode', the user ID 'uid' and the group ID 'gid' to a tar writer.  'mode'
//	must comprise four octal digits or the empty string.  If 'mode' is the
//	empty string then the object has the mode '0000'.  'uid' must be a natural
//	number or the empty string.  If 'uid' is the empty string then the object
//	has the user ID '0'.  'gid' must be a natural number or the empty string.
//	If 'gid' is the empty string then the object has the group ID '0'.
func NewDirectoryCoda(name string, mode string, uid string, gid string) (coda *Coda, err error) {
	var (
		header *tar.Header
	)

	log.Printf(`initialising add directory '%s' with mode '%s', user ID '%s' and group ID '%s'`, name, mode, uid, gid)
	header, err = NewDirectoryHeader(name, mode, uid, gid)
	if err != nil {
		return
	}

	coda = new(Coda)
	coda.Apply = func(writer *tar.Writer) (err error) {
		log.Printf(`adding directory '%s' with mode '%s', user ID '%s' and group ID '%s'`, name, mode, uid, gid)
		err = writer.WriteHeader(header)
		if err != nil {
			return
		}

		return
	}

	return
}

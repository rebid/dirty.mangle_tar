//	© 2017 Paul John King (paul_john_king@web.de).  All rights reserved.
//
//	This program is free software: you can redistribute it and/or modify it
//	under the terms of the GNU General Public License, version 3 as published
//	by the Free Software Foundation.
//
//	This program is distributed in the hope that it will be useful, but WITHOUT
//	ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//	more details.
//
//	You should have received a copy of the GNU General Public License along
//	with this program.  If not, see <http://www.gnu.org/licenses/>.

//	Notes
//	-----

//	I am entering a whole world of pain here.  There are *four* dimensions to
//	the Filter tests:
//
//	* the type of the tar object -- plain file, directory, character device,
//	  block device, fifo, hard link and symbolic link;
//
//	* the name (and possibly link name) of the tar object;
//
//	* the type of the Filter -- keep-as, keep, drop, keep-by-default and
//	  drop-by-default; and
//
//	* the pattern (and possibly replacement test) of the filter.
//
//	To preserve what sanity I can, I will start small with a single plain file
//	tar object and a single keep-as Filter.  I will then extend to different
//	obejct types, different names and different patterns.  I will finally
//	extend to different Filter types.

package mangle_tar

import (
	"fmt"
	"testing"
)

func Test_Filter_NewKeepAsFilter(t *testing.T) {
	var (
		err     error
		filter  *Filter
		report  string
		passed  bool
		success bool

		inputKeep       string
		inputAs         string
		expectedStdout  string
		expectedStderr  string
		expectedSuccess bool
	)

	err = nil
	filter = new(Filter)
	report = ``
	passed = true
	success = true

	inputKeep = `^alpha/`
	inputAs = `ALPHA`
	expectedStdout = ``
	expectedStderr = fmt.Sprintf(
		`initialising keep '%s' as '%s'`,
		inputKeep, inputAs,
	)
	expectedSuccess = true

	filter, _report, _passed, _success, err := createKeepAsFilter(
		inputKeep,
		inputAs,
		expectedStdout,
		expectedStderr,
		expectedSuccess,
	)
	if err != nil {
		t.Fatalf(`error: %s`, err.Error())
	}
	report = report + _report
	passed = passed && _passed
	success = success && _success

	if !passed {
		t.Fatal(report)
	}

	t.Log(report)

	if success {
		t.Logf("apply %s", filter.Apply)
	}

	return
}

func createKeepAsFilter(
	inputKeep string,
	inputAs string,
	expectedStdout string,
	expectedStderr string,
	expectedSuccess bool,
) (
	filter *Filter,
	report string,
	passed bool,
	success bool,
	err error,
) {
	var (
		receivedStdout  string
		receivedStderr  string
		receivedError   error
		receivedSuccess bool
	)

	receivedStdout, receivedStderr, err = CaptureStdoutStderr(
		func() {
			filter, receivedError = NewKeepAsFilter(inputKeep, inputAs)
		},
	)
	if err != nil {
		return
	}
	receivedSuccess = receivedError == nil

	report = fmt.Sprintf(`
┍━━━━━━━━━━━━━━━━┯━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
│ execute        │ NewKeepAsFilter('%s', '%s')
┝━━━━━━━━━━━━━━━━┿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
│ stdout         │
│   expected     │ '%s'
│   received     │ '%s'
│   match        │ %t
├────────────────┼────────────────────────────────────────────────
│ stderr         │
│   expected     │ '%s'
│   received     │ '%s'
│   match        │ %t
├────────────────┼────────────────────────────────────────────────
│ success        │
│   expected     │ %t
│   received     │ %t
│   match        │ %t
┕━━━━━━━━━━━━━━━━┷━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━`,
		inputKeep, inputAs,
		expectedStdout, receivedStdout, expectedStdout == receivedStdout,
		expectedStderr, receivedStderr, expectedStderr == receivedStderr,
		expectedSuccess, receivedSuccess, expectedSuccess == receivedSuccess,
	)
	passed = expectedStdout == receivedStdout &&
		expectedStderr == receivedStderr &&
		expectedSuccess == receivedSuccess
	success = expectedSuccess && receivedSuccess

	return
}

/*
© 2017 Paul John King (paul_john_king@web.de).  All rights reserved.

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License, version 3 as published by the Free
Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package mangle_tar

import (
	"encoding/xml"
	"fmt"
	"io"
)

type FilterElement struct {
	Parameters *Parameters
	Filter     *Filter
}

func (filterElement *FilterElement) UnmarshalXML(decoder *xml.Decoder, element xml.StartElement) (err error) {
	filterElement.Parameters = new(Parameters)
	filterElement.Filter = new(Filter)

	err = filterElement.UnmarshalAttributes(element)
	if err != nil {
		return
	}
	err = filterElement.UnmarshalComponents(decoder, element)
	if err != nil {
		return
	}
	err = filterElement.Postprocess(element)
	if err != nil {
		return
	}

	return
}

func (filterElement *FilterElement) UnmarshalAttributes(element xml.StartElement) (err error) {
	for _, attribute := range element.Attr {
		switch attribute.Name.Local {
		default:
			err = fmt.Errorf(
				"'%s' is not a valid attribute for element '<%s>'",
				attribute.Name.Local,
				element.Name.Local,
			)
			return
		}
	}

	return
}

func (filterElement *FilterElement) UnmarshalComponents(decoder *xml.Decoder, element xml.StartElement) (err error) {
	var (
		token xml.Token
	)

	for {
		token, err = decoder.Token()
		if err == io.EOF {
			err = nil
			break
		}
		if err != nil {
			return
		}
		switch component := token.(type) {
		case xml.CharData:
		case xml.Comment:
		case xml.EndElement:
		case xml.ProcInst:
			err = fmt.Errorf(
				"processing instruction '<?%s %s?>' is not a valid component of element '<%s>'",
				component.Target,
				component.Inst,
				element.Name.Local,
			)
			return
		case xml.StartElement:
			err = filterElement.UnmarshalChild(decoder, element, component)
			if err != nil {
				return
			}
		}
	}

	return
}

func (filterElement *FilterElement) UnmarshalChild(decoder *xml.Decoder, parent xml.StartElement, element xml.StartElement) (err error) {
	switch element.Name.Local {
	case "keep":
		err = filterElement.UnmarshalParameterChild(decoder, parent, element)
		if err != nil {
			return
		}
	case "as":
		err = filterElement.UnmarshalParameterChild(decoder, parent, element)
		if err != nil {
			return
		}
	case "drop":
		err = filterElement.UnmarshalParameterChild(decoder, parent, element)
		if err != nil {
			return
		}
	default:
		err = fmt.Errorf(
			"element '<%s>' is not a valid component of element '<%s>'",
			element.Name.Local,
			parent.Name.Local,
		)
		return
	}

	return
}

func (filterElement *FilterElement) UnmarshalParameterChild(decoder *xml.Decoder, parent xml.StartElement, element xml.StartElement) (err error) {
	var (
		parameter *Parameter
	)

	parameter = new(Parameter)
	err = decoder.DecodeElement(parameter, &element)
	if err != nil {
		return
	}
	*filterElement.Parameters = append(*filterElement.Parameters, parameter)

	return
}

func (filterElement *FilterElement) Postprocess(element xml.StartElement) (err error) {
	var (
		keepCount int
		keepValue string
		asCount   int
		asValue   string
		dropCount int
		dropValue string
	)

	for _, parameter := range *filterElement.Parameters {
		switch parameter.Name {
		case "keep":
			keepCount++
			keepValue = parameter.Value
		case "as":
			asCount++
			asValue = parameter.Value
		case "drop":
			dropCount++
			dropValue = parameter.Value
		}
	}

	switch {
	//	A 'filter' element cannot contain more than one 'keep', 'as' or 'drop' element.
	case keepCount > 1:
		err = fmt.Errorf(
			"element '<%s>' cannot contain more than one '<keep>' element",
			element.Name.Local,
		)
		return
	case asCount > 1:
		err = fmt.Errorf(
			"element '<%s>' cannot contain more than one '<as>' element",
			element.Name.Local,
		)
		return
	case dropCount > 1:
		err = fmt.Errorf(
			"element '<%s>' cannot contain more than one '<drop>' element",
			element.Name.Local,
		)
		return
	//	A 'filter' element must contain a 'keep' element or a 'drop' element, but not both.
	case keepCount > 0 && dropCount > 0:
		err = fmt.Errorf(
			"element '<%s>' cannot contain a '<keep>' element and a '<drop>' element",
			element.Name.Local,
		)
		return
	case keepCount == 0 && dropCount == 0:
		err = fmt.Errorf(
			"element '<%s>' must contain a '<keep>' element or a '<drop>' element",
			element.Name.Local,
		)
		return
	//	If a 'filter' element contains a 'keep' element then it can contain an 'as' element.
	case keepCount > 0 && asCount > 0:
		filterElement.Filter, err = NewKeepAsFilter(keepValue, asValue)
		if err != nil {
			return
		}
	case keepCount > 0 && asCount == 0:
		filterElement.Filter, err = NewKeepFilter(keepValue)
		if err != nil {
			return
		}
	//	If a 'filter' element contains a 'drop' element then it cannot contain an 'as' element.
	case dropCount > 0 && asCount > 0:
		err = fmt.Errorf(
			"element '<%s>' cannot contain a '<drop>' element and an '<as>' element",
			element.Name.Local,
		)
		return
	case dropCount > 0 && asCount == 0:
		filterElement.Filter, err = NewDropFilter(dropValue)
		if err != nil {
			return
		}
	}

	return
}

type FilterElements []*FilterElement

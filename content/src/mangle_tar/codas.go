//	© 2017 Paul John King (paul_john_king@web.de).  All rights reserved.
//
//	This program is free software: you can redistribute it and/or modify it
//	under the terms of the GNU General Public License, version 3 as published
//	by the Free Software Foundation.
//
//	This program is distributed in the hope that it will be useful, but WITHOUT
//	ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//	more details.
//
//	You should have received a copy of the GNU General Public License along
//	with this program.  If not, see <http://www.gnu.org/licenses/>.

package mangle_tar

import (
	"archive/tar"
)

//	A 'Codas' instance is a sequence of 'Coda' instances.
type Codas []*Coda

//	'Apply' tries to call the 'Apply' method of each 'Coda' instance in 'codas'
//	in sequence on the tar writer 'writer'.
func (codas *Codas) Apply(writer *tar.Writer) (err error) {
	for _, coda := range *codas {
		err = coda.Apply(writer)
		if err != nil {
			return
		}
	}

	return
}

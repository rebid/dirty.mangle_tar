/*
© 2017 Paul John King (paul_john_king@web.de).  All rights reserved.

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License, version 3 as published by the Free
Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package mangle_tar

import (
	"encoding/xml"
	"fmt"
	"io"
)

type CodaElement struct {
	CodaSet bool
	Coda    *Coda
}

func (codaElement *CodaElement) UnmarshalXML(decoder *xml.Decoder, element xml.StartElement) (err error) {
	codaElement.Coda = new(Coda)

	err = codaElement.UnmarshalAttributes(element)
	if err != nil {
		return
	}
	err = codaElement.UnmarshalComponents(decoder, element)
	if err != nil {
		return
	}
	err = codaElement.Postprocess(element)
	if err != nil {
		return
	}

	return
}

func (codaElement *CodaElement) UnmarshalAttributes(element xml.StartElement) (err error) {
	for _, attribute := range element.Attr {
		switch attribute.Name.Local {
		default:
			err = fmt.Errorf(
				"'%s' is not a valid attribute for element '<%s>'",
				attribute.Name.Local,
				element.Name.Local,
			)
			return
		}
	}

	return
}

func (codaElement *CodaElement) UnmarshalComponents(decoder *xml.Decoder, element xml.StartElement) (err error) {
	var (
		token xml.Token
	)

	for {
		token, err = decoder.Token()
		if err == io.EOF {
			err = nil
			break
		}
		if err != nil {
			return
		}
		switch component := token.(type) {
		case xml.CharData:
		case xml.Comment:
		case xml.EndElement:
		case xml.ProcInst:
			err = fmt.Errorf(
				"processing instruction '<?%s %s?>' is not a valid component of element '<%s>'",
				component.Target,
				component.Inst,
				element.Name.Local,
			)
			return
		case xml.StartElement:
			err = codaElement.UnmarshalChild(decoder, element, component)
			if err != nil {
				return
			}
		}
	}

	return
}

func (codaElement *CodaElement) UnmarshalChild(decoder *xml.Decoder, parent xml.StartElement, element xml.StartElement) (err error) {
	switch element.Name.Local {
	case "directory":
		err = codaElement.UnmarshalDirectoryChild(decoder, parent, element)
		if err != nil {
			return
		}
	default:
		err = fmt.Errorf(
			"element '<%s>' is not a valid component of element '<%s>'",
			element.Name.Local,
			parent.Name.Local,
		)
		return
	}

	return
}

func (codaElement *CodaElement) UnmarshalDirectoryChild(decoder *xml.Decoder, parent xml.StartElement, element xml.StartElement) (err error) {
	var (
		directoryElement *DirectoryElement
	)

	if codaElement.CodaSet {
		err = fmt.Errorf(
			"element '<%s>' cannot contain more than one element",
			parent.Name.Local,
		)
		return
	}
	codaElement.CodaSet = true

	directoryElement = new(DirectoryElement)
	err = decoder.DecodeElement(directoryElement, &element)
	if err != nil {
		return
	}

	codaElement.Coda = directoryElement.Coda

	return
}

func (codaElement *CodaElement) Postprocess(element xml.StartElement) (err error) {
	if !codaElement.CodaSet {
		err = fmt.Errorf(
			"element '<%s>' must contain an element",
			element.Name.Local,
		)
		return
	}

	return
}

type CodaElements []*CodaElement

type CodasElement struct {
	Codas *Codas
}

func (codasElement *CodasElement) UnmarshalXML(decoder *xml.Decoder, element xml.StartElement) (err error) {
	codasElement.Codas = new(Codas)

	err = codasElement.UnmarshalAttributes(element)
	if err != nil {
		return
	}
	err = codasElement.UnmarshalComponents(decoder, element)
	if err != nil {
		return
	}

	return
}

func (codasElement *CodasElement) UnmarshalAttributes(element xml.StartElement) (err error) {
	for _, attribute := range element.Attr {
		switch attribute.Name.Local {
		default:
			err = fmt.Errorf(
				"string '%s' is not a valid attribute for element '<%s>'",
				attribute.Name.Local,
				element.Name.Local,
			)
			return
		}
	}

	return
}

func (codasElement *CodasElement) UnmarshalComponents(decoder *xml.Decoder, element xml.StartElement) (err error) {
	var (
		token xml.Token
	)

	for {
		token, err = decoder.Token()
		if err == io.EOF {
			err = nil
			break
		}
		if err != nil {
			return
		}
		switch component := token.(type) {
		case xml.CharData:
		case xml.Comment:
		case xml.EndElement:
		case xml.ProcInst:
			err = fmt.Errorf(
				"processing instruction '<?%s %s?>' is not a valid component of element '<%s>'",
				component.Target,
				component.Inst,
				element.Name.Local,
			)
			return
		case xml.StartElement:
			err = codasElement.UnmarshalChild(decoder, element, component)
			if err != nil {
				return
			}
		}
	}

	return
}

func (codasElement *CodasElement) UnmarshalChild(decoder *xml.Decoder, parent xml.StartElement, element xml.StartElement) (err error) {
	switch element.Name.Local {
	case "coda":
		err = codasElement.UnmarshalCodaChild(decoder, parent, element)
		if err != nil {
			return
		}
	default:
		err = fmt.Errorf(
			"element '<%s>' is not a valid component of element '<%s>'",
			element.Name.Local,
			parent.Name.Local,
		)
		return
	}

	return
}

func (codasElement *CodasElement) UnmarshalCodaChild(decoder *xml.Decoder, parent xml.StartElement, element xml.StartElement) (err error) {
	var (
		codaElement *CodaElement
	)

	codaElement = new(CodaElement)
	err = decoder.DecodeElement(codaElement, &element)
	if err != nil {
		return
	}
	*codasElement.Codas = append(*codasElement.Codas, codaElement.Coda)

	return
}

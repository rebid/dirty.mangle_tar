//	© 2017 Paul John King (paul_john_king@web.de).  All rights reserved.
//
//	This program is free software: you can redistribute it and/or modify it
//	under the terms of the GNU General Public License, version 3 as published
//	by the Free Software Foundation.
//
//	This program is distributed in the hope that it will be useful, but WITHOUT
//	ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//	more details.
//
//	You should have received a copy of the GNU General Public License along
//	with this program.  If not, see <http://www.gnu.org/licenses/>.

package mangle_tar

import (
	"archive/tar"
	"fmt"
	"io"
	"testing"
)

func Test_Coda_NewDirectoryHeader(t *testing.T) {
	for _, datum := range codaData {
		t.Run(
			datum.Description,
			func(t *testing.T) {
				var (
					inputName string
					inputMode string
					inputUid  string
					inputGid  string

					expectedStdout  string
					expectedStderr  string
					expectedSuccess bool
					expectedName    string
					expectedMode    int64
					expectedUid     int
					expectedGid     int

					receivedStdout  string
					receivedStderr  string
					receivedHeader  *tar.Header
					receivedError   error
					receivedSuccess bool
					receivedName    string
					receivedMode    int64
					receivedUid     int
					receivedGid     int

					err    error
					report string
					passed bool
				)

				inputName = datum.InputName
				inputMode = datum.InputMode
				inputUid = datum.InputUid
				inputGid = datum.InputGid

				expectedStdout = ``
				expectedStderr = ``
				expectedSuccess = datum.ExpectedSuccess
				expectedName = datum.ExpectedName
				expectedMode = datum.ExpectedMode
				expectedUid = datum.ExpectedUid
				expectedGid = datum.ExpectedGid

				receivedStdout, receivedStderr, err = CaptureStdoutStderr(
					func() {
						receivedHeader, receivedError = NewDirectoryHeader(
							inputName,
							inputMode,
							inputUid,
							inputGid,
						)
					},
				)
				if err != nil {
					t.Fatalf(`error: %s`, err.Error())
				}
				receivedSuccess = receivedError == nil

				report = fmt.Sprintf(`
┍━━━━━━━━━━━━━━━━┯━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
│ execute        │ NewDirectoryHeader('%s', '%s', '%s', '%s')
┝━━━━━━━━━━━━━━━━┿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
│ stdout         │
│   expected     │ '%s'
│   received     │ '%s'
│   match        │ %t
├────────────────┼────────────────────────────────────────────────
│ stderr         │
│   expected     │ '%s'
│   received     │ '%s'
│   match        │ %t
├────────────────┼────────────────────────────────────────────────
│ success        │
│   expected     │ %t
│   received     │ %t
│   match        │ %t`,
					inputName, inputMode, inputUid, inputGid,
					expectedStdout, receivedStdout, expectedStdout == receivedStdout,
					expectedStderr, receivedStderr, expectedStderr == receivedStderr,
					expectedSuccess, receivedSuccess, expectedSuccess == receivedSuccess,
				)

				passed = expectedStdout == receivedStdout &&
					expectedStderr == receivedStderr &&
					expectedSuccess == receivedSuccess

				if passed && expectedSuccess && receivedSuccess {
					receivedName = receivedHeader.Name
					receivedMode = receivedHeader.Mode
					receivedUid = receivedHeader.Uid
					receivedGid = receivedHeader.Gid

					report = report + fmt.Sprintf(`
┝━━━━━━━━━━━━━━━━┿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
│ header name    │
│   expected     │ '%s'
│   received     │ '%s'
│   match        │ %t
├────────────────┼────────────────────────────────────────────────
│ header mode    │
│   expected     │ %04o
│   received     │ %04o
│   match        │ %t
├────────────────┼────────────────────────────────────────────────
│ header uid     │
│   expected     │ %d
│   received     │ %d
│   match        │ %t
├────────────────┼────────────────────────────────────────────────
│ gid            │
│   expected     │ %d
│   received     │ %d
│   match        │ %t`,
						expectedName, receivedName, expectedName == receivedName,
						expectedMode, receivedMode, expectedMode == receivedMode,
						expectedUid, receivedUid, expectedUid == receivedUid,
						expectedGid, receivedGid, expectedGid == receivedGid,
					)

					passed = expectedName == receivedName &&
						expectedMode == receivedMode &&
						expectedUid == receivedUid &&
						expectedGid == receivedGid
				}

				report = report + `
┕━━━━━━━━━━━━━━━━┷━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━`
				if passed {
					t.Log(report)
				} else {
					t.Fatal(report)
				}

				return
			},
		)
	}

	return
}

func Test_Coda_NewDirectoryCoda(t *testing.T) {
	for _, datum := range codaData {
		t.Run(
			datum.Description,
			func(t *testing.T) {
				var (
					inputName string
					inputMode string
					inputUid  string
					inputGid  string

					expectedStdout  string
					expectedStderr  string
					expectedSuccess bool
					expectedName    string
					expectedMode    int64
					expectedUid     int
					expectedGid     int

					receivedStdout  string
					receivedStderr  string
					receivedCoda    *Coda
					receivedError   error
					receivedSuccess bool
					receivedCount   int
					receivedName    string
					receivedMode    int64
					receivedUid     int
					receivedGid     int

					err           error
					report        string
					passed        bool
					pipeReader    io.Reader
					pipeWriter    io.Writer
					headerChannel chan *tar.Header
					tarHeader     *tar.Header
				)

				inputName = datum.InputName
				inputMode = datum.InputMode
				inputUid = datum.InputUid
				inputGid = datum.InputGid

				expectedStdout = ``
				expectedStderr = fmt.Sprintf(
					`initialising add directory '%s' with mode '%s', user ID '%s' and group ID '%s'`,
					inputName, inputMode, inputUid, inputGid,
				)
				expectedSuccess = datum.ExpectedSuccess

				receivedStdout, receivedStderr, err = CaptureStdoutStderr(
					func() {
						receivedCoda, receivedError = NewDirectoryCoda(
							inputName,
							inputMode,
							inputUid,
							inputGid,
						)
					},
				)
				if err != nil {
					t.Fatalf(`error: %s`, err.Error())
				}
				receivedSuccess = receivedError == nil

				report = fmt.Sprintf(`
┍━━━━━━━━━━━━━━━━┯━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
│ execute        │ NewDirectoryCoda('%s', '%s', '%s', '%s')
┝━━━━━━━━━━━━━━━━┿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
│ stdout         │
│   expected     │ '%s'
│   received     │ '%s'
│   match        │ %t
├────────────────┼────────────────────────────────────────────────
│ stderr         │
│   expected     │ '%s'
│   received     │ '%s'
│   match        │ %t
├────────────────┼────────────────────────────────────────────────
│ success        │
│   expected     │ %t
│   received     │ %t
│   match        │ %t`,
					inputName, inputMode, inputUid, inputGid,
					expectedStdout, receivedStdout, expectedStdout == receivedStdout,
					expectedStderr, receivedStderr, expectedStderr == receivedStderr,
					expectedSuccess, receivedSuccess, expectedSuccess == receivedSuccess,
				)

				passed = expectedStdout == receivedStdout &&
					expectedStderr == receivedStderr &&
					expectedSuccess == receivedSuccess

				if passed && expectedSuccess && receivedSuccess {
					pipeReader, pipeWriter = io.Pipe()
					headerChannel = make(chan *tar.Header)

					//	I run a "producer" routine that opens a tar writer on
					//	the pipe, calls the coda's 'Apply' method on the
					//	writer, and then closes the writer.
					go func() {
						var (
							tarWriter *tar.Writer
						)

						tarWriter = tar.NewWriter(pipeWriter)
						defer func() {
							err = tarWriter.Close()
							if err != nil {
								t.Fatalf(`error: %s`, err.Error())
							}
						}()

						receivedStdout, receivedStderr, err = CaptureStdoutStderr(
							func() {
								receivedError = receivedCoda.Apply(tarWriter)
							},
						)
						receivedSuccess = nil == receivedError

						return
					}()

					//	I run a "consumer" routine that opens a tar reader on
					//	the pipe, sends each tar header read from the reader to
					//	a channel, and then closes the channel.
					go func() {
						var (
							err         error
							tarReader   *tar.Reader
							tarHeader   *tar.Header
							objectCount int
						)

						tarReader = tar.NewReader(pipeReader)
						for {
							tarHeader, err = tarReader.Next()
							if err == io.EOF {
								err = nil
								break
							}
							if err != nil {
								t.Fatalf(`error: %s`, err.Error())
							}
							headerChannel <- tarHeader
							objectCount++
						}
						close(headerChannel)

						return
					}()

					for tarHeader = range headerChannel {
						receivedCount++
					}

					report = report + fmt.Sprintf(`
┝━━━━━━━━━━━━━━━━┿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
│ object count   │ 
│   expected     │ 1
│   received     │ %d
│   match        │ %t`,
						receivedCount, 1 == receivedCount,
					)

					passed = 1 == receivedCount

					if passed {
						expectedStdout = ``
						expectedStderr = fmt.Sprintf(
							`adding directory '%s' with mode '%s', user ID '%s' and group ID '%s'`,
							inputName, inputMode, inputUid, inputGid,
						)
						expectedSuccess = true
						expectedName = datum.ExpectedName
						expectedMode = datum.ExpectedMode
						expectedUid = datum.ExpectedUid
						expectedGid = datum.ExpectedGid

						report = report + fmt.Sprintf(`
┝━━━━━━━━━━━━━━━━┿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
│ stdout         │
│   expected     │ '%s'
│   received     │ '%s'
│   match        │ %t
├────────────────┼────────────────────────────────────────────────
│ stderr         │
│   expected     │ '%s'
│   received     │ '%s'
│   match        │ %t
├────────────────┼────────────────────────────────────────────────
│ success        │
│   expected     │ %t
│   received     │ %t
│   match        │ %t`,
							expectedStdout, receivedStdout, expectedStdout == receivedStdout,
							expectedStderr, receivedStderr, expectedStderr == receivedStderr,
							true, receivedSuccess, true == receivedSuccess,
						)

						passed = expectedStdout == receivedStdout &&
							expectedStderr == receivedStderr &&
							true == receivedSuccess

						if passed {
							receivedName = tarHeader.Name
							receivedMode = tarHeader.Mode
							receivedUid = tarHeader.Uid
							receivedGid = tarHeader.Gid

							report = report + fmt.Sprintf(`
┝━━━━━━━━━━━━━━━━┿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
│ header name    │
│   expected     │ '%s'
│   received     │ '%s'
│   match        │ %t
├────────────────┼────────────────────────────────────────────────
│ header mode    │
│   expected     │ %04o
│   received     │ %04o
│   match        │ %t
├────────────────┼────────────────────────────────────────────────
│ header uid     │
│   expected     │ %d
│   received     │ %d
│   match        │ %t
├────────────────┼────────────────────────────────────────────────
│ header gid     │
│   expected     │ %d
│   received     │ %d
│   match        │ %t`,
								expectedName, receivedName, expectedName == receivedName,
								expectedMode, receivedMode, expectedMode == receivedMode,
								expectedUid, receivedUid, expectedUid == receivedUid,
								expectedGid, receivedGid, expectedGid == receivedGid,
							)
						}
					}
				}

				report = report + `
┕━━━━━━━━━━━━━━━━┷━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━`

				if passed {
					t.Log(report)
				} else {
					t.Fatal(report)
				}
			},
		)
	}

	return
}
